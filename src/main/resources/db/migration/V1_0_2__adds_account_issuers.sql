create table account_issuers(
    id uuid primary key default uuid_generate_v4(),
    account_id uuid references accounts(id) not null,
    issuer_name varchar(50) not null,
    total_shares bigint not null,
    created_at timestamp default now(),
	updated_at timestamp default now(),
	unique(account_id, issuer_name)
);

create index on account_issuers(account_id);
create index on account_issuers(issuer_name text_pattern_ops);
create index on account_issuers(created_at);

create or replace function delete_on_update_issuer_shares()
    returns trigger as
$func$
begin
    delete from account_issuers where id = OLD.id;
    return NULL;
end
$func$ language plpgsql;

create trigger proper_delete_share
after update on account_issuers
for each row
when (NEW.total_shares = 0)
execute procedure delete_on_update_issuer_shares();