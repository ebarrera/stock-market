create index on accounts(created_at);

create table orders(
    id uuid primary key default uuid_generate_v4(),
    account_id uuid references accounts(id) not null,
    issuer_name varchar(50) not null,
    operation varchar(50) not null,
    total_shares bigint not null,
    share_price decimal(10,4) not null,
    created_at timestamp default now(),
	updated_at timestamp default now()
);

create index on orders(account_id);
create index on orders(operation text_pattern_ops);
create index on orders(issuer_name text_pattern_ops);
create index on orders(created_at);