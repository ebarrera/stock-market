--set role "stock-market";
--CREATE EXTENSION IF NOT EXISTS "uuid-ossp"; /* need to be SUPERUSER */

create or replace function public.uuid_generate_v4()
 RETURNS uuid
 LANGUAGE c
 PARALLEL SAFE STRICT
as '$libdir/uuid-ossp', $function$uuid_generate_v4$function$;

create table accounts(
    id uuid primary key default uuid_generate_v4(),
    balance decimal(10,4) not null default 0,
    status varchar(50),
    created_at timestamp default now(),
	updated_at timestamp default now()
);



