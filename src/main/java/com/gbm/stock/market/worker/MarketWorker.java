package com.gbm.stock.market.worker;

import com.gbm.stock.market.domain.MarketControlAgent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@EnableScheduling
public class MarketWorker {

    private final MarketControlAgent marketControlAgent;

    public MarketWorker(MarketControlAgent marketControlAgent) {
        this.marketControlAgent = marketControlAgent;
    }

    @Scheduled(cron = "${worker.open-market.cron}")
    public void openMarket() {
        log.info("Opening market...");
        marketControlAgent.open();
    }

    @Scheduled(cron = "${worker.close-market.cron}")
    public void closeMarket() {
        log.info("Closing market...");
        marketControlAgent.close();
    }

    @Scheduled(cron = "${worker.stay-close-market.cron}")
    public void stayCloseMarket() {
        log.info("Stay closing market...");
        marketControlAgent.close();
    }

}
