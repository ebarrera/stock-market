package com.gbm.stock.market.exception;

import static org.springframework.http.HttpStatus.NOT_MODIFIED;

public class EntityNotProcessedException extends RuntimeException {

    public EntityNotProcessedException() {
        super(NOT_MODIFIED.getReasonPhrase());
    }

    public EntityNotProcessedException(String message) {
        super(message);
    }

    public EntityNotProcessedException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
