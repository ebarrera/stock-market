package com.gbm.stock.market.exception;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

public class EntityBadRequestException extends RuntimeException {

    public EntityBadRequestException(){
        super(BAD_REQUEST.getReasonPhrase());
    }

    public EntityBadRequestException(String message){
        super(message);
    }

    public EntityBadRequestException(String message, Throwable throwable){
        super(message, throwable);
    }

}
