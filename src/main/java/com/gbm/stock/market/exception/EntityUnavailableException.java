package com.gbm.stock.market.exception;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

public class EntityUnavailableException extends RuntimeException {

    public EntityUnavailableException(){
        super(SERVICE_UNAVAILABLE.getReasonPhrase());
    }

    public EntityUnavailableException(String message){
        super(message);
    }

    public EntityUnavailableException(String message, Throwable throwable){
        super(message, throwable);
    }

}
