package com.gbm.stock.market.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(){
        super(NOT_FOUND.getReasonPhrase());
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
