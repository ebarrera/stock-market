package com.gbm.stock.market.representation.filter;

import com.gbm.stock.market.entity.Account;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class AccountFilter extends BaseFilter<Account> {
}
