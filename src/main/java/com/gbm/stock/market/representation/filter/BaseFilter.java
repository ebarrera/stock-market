package com.gbm.stock.market.representation.filter;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import javax.validation.constraints.Min;

import static java.util.Objects.isNull;
import static org.springframework.data.jpa.domain.Specification.where;

@Data
public abstract class BaseFilter<T> {

    BaseFilter() {
        calculatePages();
    }

    @Min(value = 0)
    @Getter(AccessLevel.NONE)
    private Integer page;

    @Min(value = 1)
    @Getter(AccessLevel.NONE)
    private Integer pageSize;

    public Integer getPage() {
        if (isNull(page)) {
            this.page = 0;
        }

        return page;
    }

    public Integer getPageSize() {
        if (isNull(pageSize)) {
            this.pageSize = 10;
        }

        return pageSize;
    }

    private void calculatePages() {
        getPage();
        getPageSize();
    }

    public Specification<T> toSpecification() {
        return where(null);
    }

    public PageRequest toPageRequest() {
        return PageRequest.of(page, pageSize);
    }

}
