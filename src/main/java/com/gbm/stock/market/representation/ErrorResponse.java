package com.gbm.stock.market.representation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gbm.stock.market.util.JsonUtil;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.TimeZone;

@Builder
@Getter
@EqualsAndHashCode
@ToString
public class ErrorResponse {

    private String message;

    @Builder.Default
    private LocalDateTime at = LocalDateTime.now();

    @Builder.Default
    @JsonProperty("time_zone")
    private TimeZone timeZone = TimeZone.getDefault();

    public String toJson() {
        return JsonUtil.toJson(this);
    }

}
