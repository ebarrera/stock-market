package com.gbm.stock.market.representation;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.Collections;
import java.util.List;

@Builder
@Data
public class PageRepresentation<T> {

    private List<T> content;
    private long totalSize;
    private int totalPages;
    private int pageSize;
    private int page;

    public static <T> PageRepresentation<T> toRepresentation(Page<T> page) {
        return PageRepresentation.<T>builder()
                .content(page.hasContent() ? page.getContent() : Collections.emptyList())
                .page(page.getNumber())
                .pageSize(page.getSize())
                .totalPages(page.getTotalPages())
                .totalSize(page.getTotalElements())
                .build();
    }

}
