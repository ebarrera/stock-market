package com.gbm.stock.market.representation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gbm.stock.market.domain.OperationViolation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class OrderRepresentation {

    @JsonProperty("current_balance")
    private BalanceRepresentation currentBalance;

    @JsonProperty("business_errors")
    @Builder.Default
    private List<OperationViolation> businessErrors = new ArrayList<>();

}
