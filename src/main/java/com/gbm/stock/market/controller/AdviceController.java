package com.gbm.stock.market.controller;

import com.gbm.stock.market.exception.EntityBadRequestException;
import com.gbm.stock.market.exception.EntityNotFoundException;
import com.gbm.stock.market.exception.EntityNotProcessedException;
import com.gbm.stock.market.exception.EntityUnavailableException;
import com.gbm.stock.market.representation.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.util.Optional.ofNullable;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@Slf4j
@RestControllerAdvice
public class AdviceController {

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(value = {HttpMessageNotReadableException.class})
    ErrorResponse handleBadRequestException(HttpMessageNotReadableException e, HttpServletResponse response) throws IOException {
        return ErrorResponse.builder()
                .message(validMessage(e, BAD_REQUEST))
                .build();
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(value = {IllegalArgumentException.class, EntityBadRequestException.class, MethodArgumentTypeMismatchException.class, MissingServletRequestParameterException.class})
    ErrorResponse handleBadRequestException(Exception e, HttpServletResponse response) throws IOException {
        return ErrorResponse.builder()
                .message(validMessage(e, BAD_REQUEST))
                .build();
    }

    @ResponseStatus(CONFLICT)
    @ExceptionHandler(value = {EntityNotProcessedException.class})
    ErrorResponse handleEntityNotProcessedException(EntityNotProcessedException e, HttpServletResponse response) throws IOException {
        log.warn("Details {}", e.getMessage());
        return ErrorResponse.builder()
                .message(validMessage(e, CONFLICT))
                .build();
    }

    @ResponseStatus(SERVICE_UNAVAILABLE)
    @ExceptionHandler(value = {EntityUnavailableException.class})
    ErrorResponse handleServiceUnavailable(EntityUnavailableException e, HttpServletResponse response) throws IOException {
        log.error("Details {}", e);
        return ErrorResponse.builder()
                .message(validMessage(e, SERVICE_UNAVAILABLE))
                .build();
    }

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(value = {EntityNotFoundException.class})
    ErrorResponse handleNotFoundException(EntityNotFoundException e, HttpServletResponse response) throws IOException {
        return ErrorResponse.builder()
                .message(validMessage(e, NOT_FOUND))
                .build();
    }

    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {Exception.class})
    ErrorResponse handlerException(Exception e, HttpServletResponse response) throws IOException {
        log.error("Details {}", e);
        return ErrorResponse.builder()
                .message(INTERNAL_SERVER_ERROR.getReasonPhrase())
                .build();
    }

    private String validMessage(Exception e, HttpStatus status) {
        StringBuilder builder = new StringBuilder(status.getReasonPhrase());

        return ofNullable(e.getMessage())
                .filter(StringUtils::isNotBlank)
                .map(StringBuilder::new)
                .orElse(builder)
                .toString();
    }

}
