package com.gbm.stock.market.controller;

import com.gbm.stock.market.domain.request.AccountRequest;
import com.gbm.stock.market.domain.request.OrderRequest;
import com.gbm.stock.market.entity.Account;
import com.gbm.stock.market.exception.EntityNotFoundException;
import com.gbm.stock.market.exception.EntityNotProcessedException;
import com.gbm.stock.market.representation.AccountRepresentation;
import com.gbm.stock.market.representation.OrderRepresentation;
import com.gbm.stock.market.representation.PageRepresentation;
import com.gbm.stock.market.representation.filter.AccountFilter;
import com.gbm.stock.market.service.AccountService;
import com.gbm.stock.market.service.OrderService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;

@RestController
@RequestMapping("accounts")
public class AccountController {

    private final AccountService accountService;
    private final OrderService orderService;

    public AccountController(AccountService accountService, OrderService orderService) {
        this.accountService = accountService;
        this.orderService = orderService;
    }

    @GetMapping
    public PageRepresentation<AccountRepresentation> all(AccountFilter accountFilter) {
        Page<Account> accountPage = accountService.all(accountFilter);
        List<AccountRepresentation> content = accountPage.getContent().stream()
                .map(account -> AccountRepresentation.builder()
                        .id(account.getId())
                        .cash(account.getBalance())
                        .build())
                .collect(Collectors.toList());

        var pageable = accountFilter.toPageRequest();
        var page = new PageImpl<>(content, pageable, accountPage.getTotalElements());

        return PageRepresentation.toRepresentation(page);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public AccountRepresentation create(@RequestBody AccountRequest request) {
        Account account = accountService.create(request).orElseThrow(EntityNotProcessedException::new);

        return AccountRepresentation.builder()
                .cash(account.getBalance())
                .id(account.getId())
                .build();
    }

    @PostMapping("/{account_id}/orders")
    public ResponseEntity<OrderRepresentation> order(@PathVariable UUID account_id, @RequestBody OrderRequest request) {
        Account account = accountService.findBy(account_id).orElseThrow(EntityNotFoundException::new);
        AccountRepresentation accountRepresentation = AccountRepresentation.builder()
                .cash(account.getBalance())
                .id(account.getId())
                .build();
        OrderRepresentation representation = orderService.create(accountRepresentation, request);

        if (CollectionUtils.isNotEmpty(representation.getBusinessErrors())) {
            return new ResponseEntity<>(representation, NOT_ACCEPTABLE);
        }

        return new ResponseEntity<>(representation, CREATED);
    }

}
