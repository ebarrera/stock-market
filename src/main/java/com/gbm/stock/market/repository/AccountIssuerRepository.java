package com.gbm.stock.market.repository;

import com.gbm.stock.market.entity.AccountIssuer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public interface AccountIssuerRepository extends JpaRepository<AccountIssuer, UUID> {

    List<AccountIssuer> findByAccountId(UUID accountId);

    default Map<String, AccountIssuer> findByAccountIdMap(UUID accountId) {
        return findByAccountId(accountId).stream()
                .collect(Collectors.toMap(AccountIssuer::getIssuerName, issuer -> issuer));
    }

}
