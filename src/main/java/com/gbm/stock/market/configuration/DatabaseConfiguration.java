package com.gbm.stock.market.configuration;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfiguration {

    @Value("${jdbc.driver-classname}")
    private String driverClassName;

    @Value("${jdbc.url}")
    private String jdbcUrlWrite;

    @Value("${jdbc.username}")
    private String usernameWrite;

    @Value("${jdbc.password}")
    private String passwordWrite;

    @Value("${jdbc.max-pool-size}")
    private Integer maximumPoolSizeWrite;

    @Value("${jdbc.init-query}")
    private String connectionTestQueryWrite;

    @Value("${jdbc.leak-detection-threshold}")
    private Long leakDetectionThresholdWrite;

    @Bean
    public DataSource dataSource() {
        return buildDataSource(jdbcUrlWrite,
                usernameWrite,
                passwordWrite,
                maximumPoolSizeWrite,
                connectionTestQueryWrite,
                leakDetectionThresholdWrite,
                false);
    }

    private DataSource buildDataSource(String url, String username, String password, Integer maximumPoolSize,
                                       String connectionTestQuery, Long leakDetectionThreshold, Boolean readOnly) {
        HikariDataSource dataSource = new HikariDataSource();

        dataSource.setDriverClassName(driverClassName);
        dataSource.setJdbcUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setMaximumPoolSize(maximumPoolSize);
        dataSource.setReadOnly(readOnly);

        if (connectionTestQuery != null) {
            dataSource.setConnectionTestQuery(connectionTestQuery);
        }

        if (leakDetectionThreshold != null) {
            dataSource.setLeakDetectionThreshold(leakDetectionThreshold);
        }

        return dataSource;
    }

}
