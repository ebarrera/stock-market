package com.gbm.stock.market.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.TreeMap;

@Configuration
public class RepeatedConfiguration {

    // TODO improve with clustered cache
    @Bean
    public TreeMap<String, Integer> mapCached() {
        return new TreeMap<>();
    }
}
