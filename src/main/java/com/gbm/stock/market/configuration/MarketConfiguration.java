package com.gbm.stock.market.configuration;

import com.gbm.stock.market.domain.MarketControlAgent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MarketConfiguration {

    @Bean
    public MarketControlAgent marketControlAgent() {
        return new MarketControlAgent();
    }

}
