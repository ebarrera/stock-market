package com.gbm.stock.market.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.collect.ImmutableSet;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
public class JsonUtil {

    public static Hibernate5Module hibernate5Module() {
        return new Hibernate5Module()
                .enable(Hibernate5Module.Feature.SERIALIZE_IDENTIFIER_FOR_LAZY_NOT_LOADED_OBJECTS)
                .disable(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION);
    }

    public static ObjectMapper mapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule())
                .registerModule(hibernate5Module())
                .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, false)
                .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        return mapper;
    }

    @SneakyThrows
    public static <R> String toJson(ObjectWriter writer, R representation) {
        return writer.writeValueAsString(representation);
    }

    public static <R> String toJson(R representation) {
        return toJson(mapper().writer(), representation);
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        ObjectMapper mapper = mapper();
        return fromJson(mapper.readerFor(clazz), json);
    }

    @SneakyThrows
    public static <T> T fromJson(ObjectReader reader, String json) {
        return reader.readValue(json);
    }

    @SneakyThrows
    public static <T> List<T> fromJsonToList(String json, Class<T> clazz) {
        ObjectMapper mapper = mapper();
        return mapper.readValue(json, TypeFactory.defaultInstance().constructCollectionType(List.class, clazz));
    }

    public static <R> String toSensitiveJson(R representation, Map<Type, Set<String>> excludedPropertiesByClass) {
        return toJson(
                JacksonUtil.filterEnabledObjectWriter(mapper(), excludedPropertiesByClass), representation);
    }

    public static <R> String toSensitiveJson(R representation, Set<String> excludedProperties) {
        return toJson(
                JacksonUtil.filterEnabledObjectWriter(mapper(), excludedProperties), representation);
    }

    public static <R> String toSensitiveJson(R representation, String... excludedProperties) {
        return toSensitiveJson(representation, ImmutableSet.copyOf(excludedProperties));
    }

}
