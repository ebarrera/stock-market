package com.gbm.stock.market.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;

public class JacksonUtil {

    private JacksonUtil() {
    }

    public static ObjectWriter filterEnabledObjectWriter(ObjectMapper objectMapper,
                                                         Map<Type, Set<String>> excludedPropertiesByClass,
                                                         Set<String> excludedPropertiesAllClasses) {

        // Enables filter for all classes, not only classes with @JsonFilter
        objectMapper.setAnnotationIntrospector(new JacksonAnnotationIntrospector() {
            @Override
            public Object findFilterId(Annotated a) {
                // Filter name will be the fully qualified class name (com.package.ClassName)
                if (a.getAnnotated() instanceof Class) {
                    return ((Class) a.getAnnotated()).getTypeName();
                } else {
                    return null;
                }
            }
        });

        SimpleFilterProvider filterProvider = new SimpleFilterProvider()
                .setDefaultFilter(SimpleBeanPropertyFilter.serializeAll())
                .setFailOnUnknownId(false);

        // If there's a default filter, sets it
        if (!excludedPropertiesAllClasses.isEmpty()) {
            filterProvider.setDefaultFilter(SimpleBeanPropertyFilter.serializeAllExcept(excludedPropertiesAllClasses));
        }

        excludedPropertiesByClass.forEach((type, properties) ->
                filterProvider.addFilter(type.getTypeName(), SimpleBeanPropertyFilter.serializeAllExcept(properties))
        );

        return objectMapper.writer(filterProvider);
    }

    public static ObjectWriter filterEnabledObjectWriter(ObjectMapper objectMapper, Set<String> defaultExcludedProperties) {
        return filterEnabledObjectWriter(objectMapper, emptyMap(), defaultExcludedProperties);
    }

    public static ObjectWriter filterEnabledObjectWriter(ObjectMapper objectMapper, Map<Type, Set<String>> excludedPropertiesByClass) {
        return filterEnabledObjectWriter(objectMapper, excludedPropertiesByClass, emptySet());
    }
}
