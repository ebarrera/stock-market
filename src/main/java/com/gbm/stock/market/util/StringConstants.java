package com.gbm.stock.market.util;

public final class StringConstants {

    private StringConstants(){}

    public static final String EMPTY = "";
    public static final String COMMA = ",";
    public static final String SPACE = " ";
    public static final String URL_SEPARATOR = "/";
    public static final String EQUAL = "=";
    public static final String AMPERSAND = "&";
    public static final String QUESTION_MARK = "?";
    public static final String ASTERISK = "*";

}
