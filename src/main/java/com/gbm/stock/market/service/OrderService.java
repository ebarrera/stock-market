package com.gbm.stock.market.service;

import com.gbm.stock.market.domain.Gateway;
import com.gbm.stock.market.domain.OperationReady;
import com.gbm.stock.market.domain.OrderReady;
import com.gbm.stock.market.domain.request.OrderRequest;
import com.gbm.stock.market.entity.Order;
import com.gbm.stock.market.repository.OrderRepository;
import com.gbm.stock.market.representation.AccountRepresentation;
import com.gbm.stock.market.representation.BalanceRepresentation;
import com.gbm.stock.market.representation.IssuerRepresentation;
import com.gbm.stock.market.representation.OrderRepresentation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrderService {

    private OrderRepository orderRepository;
    private final Gateway gateway;

    public OrderService(OrderRepository orderRepository, Gateway gateway) {
        this.orderRepository = orderRepository;
        this.gateway = gateway;
    }

    public OrderRepresentation create(AccountRepresentation account, OrderRequest request) {
        OperationReady operationReady = gateway.compute(account, request);
        OrderRepresentation.OrderRepresentationBuilder builder = OrderRepresentation.builder();
        OrderReady prepareOrder = gateway.prepareOperation(operationReady);
        List<Order> orders = new ArrayList<>();

        if (prepareOrder.isReady()) {
            orders = prepareOrder.getIssuers().stream()
                    .map(issuer -> Order.builder()
                            .accountId(prepareOrder.getAccountId())
                            .operation(operationReady.getOperation())
                            .issuerName(issuer.getIssuerName())
                            .totalShares(issuer.getTotalShares())
                            .sharePrice(issuer.getSharePrice())
                            .build())
                    .collect(Collectors.toList());
        } else {
            log.error("Some error {}", operationReady);
            builder.businessErrors(operationReady.getViolations());
        }

        List<IssuerRepresentation> issuers = orderRepository.saveAll(orders).stream()
                .map(order -> IssuerRepresentation.builder()
                        .issuerName(order.getIssuerName())
                        .sharePrice(order.getSharePrice())
                        .totalShares(order.getTotalShares())
                        .build())
                .collect(Collectors.toList());

        builder.currentBalance(BalanceRepresentation.builder()
                .cash(prepareOrder.getBalance())
                .issuers(issuers)
                .build());

        return builder.build();
    }

}
