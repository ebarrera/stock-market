package com.gbm.stock.market.service;

import com.gbm.stock.market.domain.request.AccountRequest;
import com.gbm.stock.market.entity.Account;
import com.gbm.stock.market.entity.AccountIssuer;
import com.gbm.stock.market.repository.AccountIssuerRepository;
import com.gbm.stock.market.repository.AccountRepository;
import com.gbm.stock.market.representation.filter.AccountFilter;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static com.gbm.stock.market.representation.AccountStatus.CREATED;
import static java.util.Optional.ofNullable;

@Service
public class AccountService {

    private final AccountRepository accountRepository;
    private final AccountIssuerRepository accountIssuerRepository;

    public AccountService(AccountRepository accountRepository, AccountIssuerRepository accountIssuerRepository) {
        this.accountRepository = accountRepository;
        this.accountIssuerRepository = accountIssuerRepository;
    }

    public Optional<Account> save(Account account) {
        Account saved = accountRepository.save(account);
        return ofNullable(saved);
    }

    public Optional<Account> create(AccountRequest request) {
        Account account = Account.builder()
                .balance(request.getCash())
                .status(CREATED)
                .build();

        return save(account);
    }

    public Map<String, AccountIssuer> allIssuersBy(UUID accountId) {
        return accountIssuerRepository.findByAccountIdMap(accountId);
    }

    public List<AccountIssuer> saveIssuers(List<AccountIssuer> issuers) {
        return accountIssuerRepository.saveAll(issuers);
    }

    public Optional<Account> findBy(UUID id) {
        return accountRepository.findById(id);
    }

    public Page<Account> all(AccountFilter filter) {
        return accountRepository.findAll(filter.toSpecification(), filter.toPageRequest());
    }
}
