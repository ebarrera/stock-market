package com.gbm.stock.market.domain.chain.account;

import com.gbm.stock.market.domain.OperationViolation;
import com.gbm.stock.market.domain.chain.Interceptor;
import com.gbm.stock.market.domain.chain.OperationContext;
import com.gbm.stock.market.domain.request.IssuerRequest;
import com.gbm.stock.market.representation.AccountRepresentation;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

import static com.gbm.stock.market.domain.TransactionOperation.BUY;

@Component
public class AccountBalanceInterceptor extends Interceptor {

    @Override
    public OperationContext check(AccountRepresentation account, OperationContext context) {
        BigDecimal orderPrice = computeOrderPrice(context);

        if(context.getRequest().getOperation() == BUY){
            if (account.getCash().compareTo(orderPrice) < 0) {
                List<OperationViolation> violations = context.getViolations();
                violations.add(OperationViolation.INSUFFICIENT_BALANCE);

                context = context.toBuilder()
                        .violations(violations)
                        .build();
            }
        }

        return checkNext(account, context);
    }

    private BigDecimal computeOrderPrice(OperationContext operationContext) {
        List<IssuerRequest> issuers = operationContext.getRequest().getIssuers();

        return issuers.stream()
                .map(issuer -> {
                    Long totalShares = issuer.getTotalShares();
                    BigDecimal price = issuer.getSharePrice();
                    return price.multiply(BigDecimal.valueOf(totalShares));
                })
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
