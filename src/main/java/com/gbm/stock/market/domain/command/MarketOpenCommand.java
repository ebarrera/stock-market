package com.gbm.stock.market.domain.command;

import com.gbm.stock.market.domain.MarketOperation;

public class MarketOpenCommand implements Command {

    private final MarketOperation marketOperation;

    public MarketOpenCommand(MarketOperation marketOperation) {
        this.marketOperation = marketOperation;
    }

    @Override
    public void execute() {
        marketOperation.open();
    }
}
