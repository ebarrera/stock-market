package com.gbm.stock.market.domain.command;

public interface Command {

    void execute();

}
