package com.gbm.stock.market.domain;

import com.gbm.stock.market.domain.command.Command;
import com.gbm.stock.market.domain.command.MarketCloseCommand;
import com.gbm.stock.market.domain.command.MarketOpenCommand;

public class MarketControlAgent {

    private final Command openMarket;
    private final Command closeMarket;
    private final MarketOperation marketOperation;

    public MarketControlAgent() {
        this.marketOperation = new MarketOperation();
        this.openMarket = new MarketOpenCommand(marketOperation);
        this.closeMarket = new MarketCloseCommand(marketOperation);
    }

    public void open() {
        openMarket.execute();
    }

    public void close() {
        closeMarket.execute();
    }

    public boolean isOpen() {
        return marketOperation.isOpen();
    }

}
