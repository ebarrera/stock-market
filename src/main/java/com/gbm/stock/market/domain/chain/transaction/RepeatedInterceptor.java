package com.gbm.stock.market.domain.chain.transaction;

import com.gbm.stock.market.domain.OperationViolation;
import com.gbm.stock.market.domain.chain.Interceptor;
import com.gbm.stock.market.domain.chain.OperationContext;
import com.gbm.stock.market.domain.request.IssuerRequest;
import com.gbm.stock.market.representation.AccountRepresentation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import static java.math.BigDecimal.ZERO;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;

@Slf4j
@Component
public class RepeatedInterceptor extends Interceptor {

    final TreeMap<String, Integer> mapCached;
    private final int maxRequest;
    private long currentTime;

    public RepeatedInterceptor(TreeMap<String, Integer> mapCached) {
        this.mapCached = mapCached;
        this.maxRequest = 2;
        this.currentTime = System.currentTimeMillis();
    }

    @Override
    public OperationContext check(AccountRepresentation account, OperationContext context) {
        UUID accountId = account.getId();

        // Interval 5 minutes
        if (System.currentTimeMillis() > currentTime + 30000) {
            deleteAccountIndexes(accountId);
            currentTime = System.currentTimeMillis();
        }

        Map<String, BigDecimal> issuers = context.getRequest().getIssuers().stream()
                .collect(groupingBy(IssuerRequest::getIssuerName, reducing(ZERO, IssuerRequest::getSharePrice, BigDecimal::add)));

        List<OperationViolation> violations = context.getViolations();

        for (Map.Entry<String, BigDecimal> entry : issuers.entrySet()) {
            String issuerName = entry.getKey();
            BigDecimal amount = entry.getValue();

            // Ex. key = UUID|APPL|350
            String composeKey = accountId.toString().concat("|").concat(issuerName).concat("|").concat(amount.toString());

            if (mapCached.containsKey(composeKey)) {
                Integer value = mapCached.get(composeKey);
                int newValue = value + 1;
                mapCached.put(composeKey, newValue);

                if (newValue >= maxRequest) {
                    violations.add(OperationViolation.DUPLICATED_OPERATION);
                    context = context.toBuilder().violations(violations).build();
                    break;
                }
            } else {
                mapCached.put(composeKey, 1);
            }
        }

        return checkNext(account, context);
    }

    private void deleteAccountIndexes(UUID accountId) {
        SortedMap<String, Integer> filter = getByPrefix(mapCached, accountId.toString());
        mapCached.entrySet().removeIf(entry -> filter.containsKey(entry.getKey()));
    }

    private static SortedMap<String, Integer> getByPrefix(NavigableMap<String, Integer> myMap, String prefix) {
        return myMap.subMap(prefix, prefix + Character.MAX_VALUE);
    }

}
