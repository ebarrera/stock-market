package com.gbm.stock.market.domain.chain;

import com.gbm.stock.market.representation.AccountRepresentation;

public abstract class Interceptor {

    private Interceptor next;

    public Interceptor with(Interceptor next) {
        this.next = next;
        return next;
    }

    public abstract OperationContext check(AccountRepresentation account, OperationContext context);

    protected OperationContext checkNext(AccountRepresentation account, OperationContext context) {
        if (next == null) {
            return context;
        }

        return next.check(account, context);
    }
}
