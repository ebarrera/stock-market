package com.gbm.stock.market.domain;

import com.gbm.stock.market.representation.IssuerRepresentation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class OrderReady {

    private UUID accountId;
    private BigDecimal balance;
    private boolean ready;
    private List<IssuerRepresentation> issuers;

}
