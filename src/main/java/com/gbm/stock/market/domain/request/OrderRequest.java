package com.gbm.stock.market.domain.request;

import com.gbm.stock.market.domain.TransactionOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class OrderRequest {

    private TransactionOperation operation;
    private List<IssuerRequest> issuers;

}
