package com.gbm.stock.market.domain.chain;

import com.gbm.stock.market.representation.AccountRepresentation;
import org.springframework.stereotype.Component;

@Component
public class GenericInterceptor {

    public CheckOperation on(AccountRepresentation account, Interceptor interceptor) {
        return context -> check(account, interceptor, context);
    }

    private OperationContext check(AccountRepresentation account, Interceptor interceptor, OperationContext context) {
        return interceptor.check(account, context);
    }

}
