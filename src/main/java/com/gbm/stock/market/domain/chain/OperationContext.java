package com.gbm.stock.market.domain.chain;

import com.gbm.stock.market.domain.OperationViolation;
import com.gbm.stock.market.domain.request.OrderRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class OperationContext {

    private OrderRequest request;

    @Builder.Default
    private final List<OperationViolation> violations = new ArrayList<>();

}
