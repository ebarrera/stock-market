package com.gbm.stock.market.domain.chain.account;

import com.gbm.stock.market.domain.OperationViolation;
import com.gbm.stock.market.domain.TransactionOperation;
import com.gbm.stock.market.domain.chain.Interceptor;
import com.gbm.stock.market.domain.chain.OperationContext;
import com.gbm.stock.market.domain.request.IssuerRequest;
import com.gbm.stock.market.entity.AccountIssuer;
import com.gbm.stock.market.representation.AccountRepresentation;
import com.gbm.stock.market.service.AccountService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static com.gbm.stock.market.domain.TransactionOperation.SELL;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingLong;

@Component
public class AccountIssuerInterceptor extends Interceptor {

    private final AccountService accountService;

    public AccountIssuerInterceptor(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public OperationContext check(AccountRepresentation account, OperationContext context) {
        TransactionOperation operation = context.getRequest().getOperation();

        if (operation == SELL) {
            Map<String, Long> requestIssuers = context.getRequest().getIssuers().stream()
                    .collect(groupingBy(IssuerRequest::getIssuerName, summingLong(IssuerRequest::getTotalShares)));
            Map<String, AccountIssuer> issuers = accountService.allIssuersBy(account.getId());
            List<OperationViolation> violations = context.getViolations();

            for (Map.Entry<String, Long> entry : requestIssuers.entrySet()) {
                String issuer = entry.getKey();
                Long quantity = entry.getValue();
                AccountIssuer accountIssuer = issuers.get(issuer);

                if (accountIssuer == null) {
                    violations.add(OperationViolation.INSUFFICIENT_STOCKS);
                    context = context.toBuilder().violations(violations).build();
                    break;
                } else {
                    Long storedShares = accountIssuer.getTotalShares();

                    if (quantity > storedShares) {
                        violations.add(OperationViolation.INSUFFICIENT_STOCKS);
                        context = context.toBuilder().violations(violations).build();
                        break;
                    }
                }
            }
        }

        return checkNext(account, context);
    }

}
