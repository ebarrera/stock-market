package com.gbm.stock.market.domain;

public enum OperationViolation {

    INSUFFICIENT_BALANCE,
    INSUFFICIENT_STOCKS,
    DUPLICATED_OPERATION,
    CLOSED_MARKET,
    INVALID_OPERATION

}
