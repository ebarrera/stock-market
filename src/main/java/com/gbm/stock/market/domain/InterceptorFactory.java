package com.gbm.stock.market.domain;

import com.gbm.stock.market.domain.chain.Interceptor;
import com.gbm.stock.market.domain.chain.account.AccountBalanceInterceptor;
import com.gbm.stock.market.domain.chain.account.AccountIssuerInterceptor;
import com.gbm.stock.market.domain.chain.market.MarketScheduleInterceptor;
import com.gbm.stock.market.domain.chain.transaction.RepeatedInterceptor;
import org.springframework.stereotype.Component;

@Component
public class InterceptorFactory {

    private final MarketScheduleInterceptor marketScheduleInterceptor;
    private final AccountBalanceInterceptor accountBalanceInterceptor;
    private final AccountIssuerInterceptor accountIssuerInterceptor;
    private final RepeatedInterceptor repeatedInterceptor;

    public InterceptorFactory(MarketScheduleInterceptor marketScheduleInterceptor, AccountBalanceInterceptor accountBalanceInterceptor,
                              AccountIssuerInterceptor accountIssuerInterceptor, RepeatedInterceptor repeatedInterceptor) {
        this.marketScheduleInterceptor = marketScheduleInterceptor;
        this.accountBalanceInterceptor = accountBalanceInterceptor;
        this.accountIssuerInterceptor = accountIssuerInterceptor;
        this.repeatedInterceptor = repeatedInterceptor;
    }

    public Interceptor basicInterceptor() {
        Interceptor interceptor = marketScheduleInterceptor;
        interceptor.with(accountBalanceInterceptor)
                .with(accountIssuerInterceptor)
                .with(repeatedInterceptor);
        return interceptor;
    }

}
