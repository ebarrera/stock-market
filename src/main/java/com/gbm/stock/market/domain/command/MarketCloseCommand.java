package com.gbm.stock.market.domain.command;

import com.gbm.stock.market.domain.MarketOperation;

public class MarketCloseCommand implements Command {

    private final MarketOperation marketOperation;

    public MarketCloseCommand(MarketOperation marketOperation) {
        this.marketOperation = marketOperation;
    }

    @Override
    public void execute() {
        marketOperation.close();
    }
}
