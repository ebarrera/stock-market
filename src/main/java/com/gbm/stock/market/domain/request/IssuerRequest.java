package com.gbm.stock.market.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class IssuerRequest {

    @JsonProperty("issuer_name")
    private String issuerName;

    @JsonProperty("total_shares")
    private Long totalShares;

    @JsonProperty("share_price")
    private BigDecimal sharePrice;

}
