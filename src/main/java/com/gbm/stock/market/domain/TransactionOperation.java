package com.gbm.stock.market.domain;

import java.math.BigDecimal;
import java.util.function.BiFunction;

public enum TransactionOperation {

    BUY(BigDecimal::subtract, Long::sum),
    SELL(BigDecimal::add, (x, y) -> x - y);

    private final BiFunction<BigDecimal, BigDecimal, BigDecimal> balanceStrategy;
    private final BiFunction<Long, Long, Long> stockStrategy;

    TransactionOperation(BiFunction<BigDecimal, BigDecimal, BigDecimal> balanceStrategy, BiFunction<Long, Long, Long> stockStrategy) {
        this.balanceStrategy = balanceStrategy;
        this.stockStrategy = stockStrategy;
    }

    public BigDecimal strategy(BigDecimal balance, BigDecimal amount) {
        return balanceStrategy.apply(balance, amount);
    }

    public Long strategy(Long shares, Long quantity) {
        return stockStrategy.apply(shares, quantity);
    }
}
