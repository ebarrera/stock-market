package com.gbm.stock.market.domain;

import com.gbm.stock.market.representation.IssuerRepresentation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class OperationReady {

    private UUID accountId;
    private List<IssuerRepresentation> issuers;
    private TransactionOperation operation;

    @Builder.Default
    private final List<OperationViolation> violations = new ArrayList<>();

}
