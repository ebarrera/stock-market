package com.gbm.stock.market.domain.operation;

import com.gbm.stock.market.domain.OperationReady;
import com.gbm.stock.market.domain.OrderReady;
import com.gbm.stock.market.domain.TransactionOperation;
import com.gbm.stock.market.entity.Account;
import com.gbm.stock.market.exception.EntityNotFoundException;
import com.gbm.stock.market.representation.IssuerRepresentation;
import com.gbm.stock.market.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Slf4j
@Component
public class UpdateBalanceOperation implements Operation {

    private final AccountService accountService;

    public UpdateBalanceOperation(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public OrderReady execute(OperationReady operationReady) {
        UUID accountId = operationReady.getAccountId();

        Account account = accountService.findBy(accountId).orElseThrow(EntityNotFoundException::new);
        TransactionOperation operation = operationReady.getOperation();

        BigDecimal balance = account.getBalance();
        BigDecimal orderPrice = computeOrderPrice(operationReady);
        BigDecimal newBalance = balance;
        boolean readyToOperate = false;

        if (operationReady.getViolations().size() == 0) {
            newBalance = operation.strategy(balance, orderPrice);
            readyToOperate = true;
        }

        log.info("Operation [{}] with current balance [{}] for order price [{}]", operation, newBalance, orderPrice);
        account = account.toBuilder().balance(newBalance).build();
        accountService.save(account);

        return OrderReady.builder()
                .balance(newBalance)
                .ready(readyToOperate)
                .accountId(accountId)
                .issuers(operationReady.getIssuers())
                .build();
    }

    private BigDecimal computeOrderPrice(OperationReady operationReady) {
        List<IssuerRepresentation> issuers = operationReady.getIssuers();

        return issuers.stream()
                .map(issuer -> {
                    Long totalShares = issuer.getTotalShares();
                    BigDecimal price = issuer.getSharePrice();
                    return price.multiply(BigDecimal.valueOf(totalShares));
                })
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
