package com.gbm.stock.market.domain.operation;

import com.gbm.stock.market.domain.OperationReady;
import com.gbm.stock.market.domain.OrderReady;

public interface Operation {

    OrderReady execute(OperationReady operationReady);

}
