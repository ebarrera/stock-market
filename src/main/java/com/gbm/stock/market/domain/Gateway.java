package com.gbm.stock.market.domain;

import com.gbm.stock.market.domain.chain.GenericInterceptor;
import com.gbm.stock.market.domain.chain.Interceptor;
import com.gbm.stock.market.domain.chain.OperationContext;
import com.gbm.stock.market.domain.operation.StockOperation;
import com.gbm.stock.market.domain.operation.UpdateBalanceOperation;
import com.gbm.stock.market.domain.request.OrderRequest;
import com.gbm.stock.market.representation.AccountRepresentation;
import com.gbm.stock.market.representation.IssuerRepresentation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class Gateway {

    private final GenericInterceptor genericInterceptor;
    private final InterceptorFactory interceptorFactory;
    private final UpdateBalanceOperation updateBalanceOperation;
    private final StockOperation stockOperation;

    public Gateway(GenericInterceptor genericInterceptor, InterceptorFactory interceptorFactory, UpdateBalanceOperation updateBalanceOperation,
                   StockOperation stockOperation) {
        this.genericInterceptor = genericInterceptor;
        this.interceptorFactory = interceptorFactory;
        this.updateBalanceOperation = updateBalanceOperation;
        this.stockOperation = stockOperation;
    }

    public OperationReady compute(AccountRepresentation account, OrderRequest request) {
        Interceptor interceptor = interceptorFactory.basicInterceptor();
        OperationContext context = OperationContext.builder()
                .request(request)
                .build();

        OperationContext contextToBeReady = genericInterceptor.on(account, interceptor).check(context);

        List<IssuerRepresentation> issuers = contextToBeReady.getRequest().getIssuers().stream()
                .map(issuer -> IssuerRepresentation.builder()
                        .issuerName(issuer.getIssuerName())
                        .totalShares(issuer.getTotalShares())
                        .sharePrice(issuer.getSharePrice())
                        .build())
                .collect(Collectors.toList());

        return OperationReady.builder()
                .accountId(account.getId())
                .operation(contextToBeReady.getRequest().getOperation())
                .issuers(issuers)
                .violations(contextToBeReady.getViolations())
                .build();
    }

    public OrderReady prepareOperation(OperationReady operationReady) {
        OrderReady orderReady = updateBalanceOperation.execute(operationReady);

        if (operationReady.getViolations().size() == 0) {
            stockOperation.compute(operationReady.getOperation(), orderReady);
        }

        return orderReady;
    }
}
