package com.gbm.stock.market.domain.chain;

@FunctionalInterface
public interface CheckOperation {

    OperationContext check(OperationContext context);

}
