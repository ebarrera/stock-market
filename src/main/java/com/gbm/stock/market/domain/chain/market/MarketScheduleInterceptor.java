package com.gbm.stock.market.domain.chain.market;

import com.gbm.stock.market.domain.MarketControlAgent;
import com.gbm.stock.market.domain.OperationViolation;
import com.gbm.stock.market.domain.chain.Interceptor;
import com.gbm.stock.market.domain.chain.OperationContext;
import com.gbm.stock.market.representation.AccountRepresentation;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MarketScheduleInterceptor extends Interceptor {

    private final MarketControlAgent marketControlAgent;

    public MarketScheduleInterceptor(MarketControlAgent marketControlAgent) {
        this.marketControlAgent = marketControlAgent;
    }

    @Override
    public OperationContext check(AccountRepresentation account, OperationContext context) {

        if (marketControlAgent.isOpen()) {
            return checkNext(account, context);
        }

        List<OperationViolation> violations = context.getViolations();
        violations.add(OperationViolation.CLOSED_MARKET);

        context = context.toBuilder()
                .violations(violations)
                .build();

        return checkNext(account, context);
    }
}
