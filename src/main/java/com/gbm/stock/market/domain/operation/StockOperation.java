package com.gbm.stock.market.domain.operation;

import com.gbm.stock.market.domain.OrderReady;
import com.gbm.stock.market.domain.TransactionOperation;
import com.gbm.stock.market.entity.AccountIssuer;
import com.gbm.stock.market.representation.IssuerRepresentation;
import com.gbm.stock.market.service.AccountService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingLong;

@Component
public class StockOperation {

    private final AccountService accountService;

    public StockOperation(AccountService accountService) {
        this.accountService = accountService;
    }

    public void compute(TransactionOperation operation, OrderReady orderReady) {
        UUID accountId = orderReady.getAccountId();

        Map<String, Long> issuers = orderReady.getIssuers().stream()
                .collect(groupingBy(IssuerRepresentation::getIssuerName, summingLong(IssuerRepresentation::getTotalShares)));
        Map<String, AccountIssuer> accountIssuers = accountService.allIssuersBy(accountId);

        List<AccountIssuer> toSave = new ArrayList<>();

        issuers.forEach((issuerName, quantity) -> {
            AccountIssuer issuer = accountIssuers.get(issuerName);

            if (issuer == null) {
                AccountIssuer accountIssuer = AccountIssuer.builder()
                        .accountId(accountId)
                        .issuerName(issuerName)
                        .totalShares(quantity)
                        .build();
                toSave.add(accountIssuer);
            } else {
                Long storedShares = issuer.getTotalShares();
                issuer = issuer.toBuilder()
                        .totalShares(operation.strategy(storedShares, quantity))
                        .build();
                toSave.add(issuer);
            }
        });

        accountService.saveIssuers(toSave);
    }

}
