package com.gbm.stock.market.domain;

import lombok.Getter;

@Getter
public class MarketOperation {

    private boolean open;

    public void open() {
        this.open = true;
    }

    public void close() {
        this.open = false;
    }

}
