FROM maven:3.6.1-jdk-11-slim

WORKDIR /app

# Prepare by downloading dependecies
ADD pom.xml /app/pom.xml
RUN mvn dependency:resolve

ADD src /app/src
RUN mvn package

COPY scripts/wait-for-it.sh /app/wait-for-it.sh
ADD scripts/run.sh /app/run.sh

ENTRYPOINT /app/run.sh