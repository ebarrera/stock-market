# GBM challenge

#### A little beginning ####

>If at first you don't succeed, call it version 1.0 __~ Erick Barrera__

#### Author ####
* Erick Barrera - **ebarreral.isc@gmail.com**

### Macro to save and build (IntelliJ)

Associate with the `command + shift + s` shortcut a macro with the commands:

* File > Save all
* Build > Make project

[How to create a macro on IntelliJ](https://www.jetbrains.com/help/idea/using-macros-in-the-editor.html)

**Why do I need to do this?**

Because devtools notices the changes in the classpath and does the load - ClassLoader,
only what has changed. But in Intellij the code is not fully compiled
every time the project is saved - this occurs automatically from times in
times, which disrupts the devtools, so I helped the devtools by doing the
save chain a total project build in intellij.

### Lombok and Annotation Processing

Since the project uses Lombok, you will need to make two settings in the IDE:

1. Check _Enable Annotation Processing_ in:

  ```
  Preferences > Build > Compiler > Annotation Processor
  ```

2. Install the Lombok plugin on:

  ```
  Preference > Plugins > Browse Repositories > Lombok Plugin
  ```

[Lombok mirror IntelliJ](https://plugins.jetbrains.com/plugin/6317-lombok/versions)

### Technology stack ###

**Base**

- Java 11
- Docker
- Maven
- Spring

**Some design patterns used:**

- Chain of responsibility
- Command
- Gateway
- Singleton
- Dependency injection

### How to run the app?

#### Important: To compile this project, you need the 11 JDK version

1- Run the docker compose

````
$> docker-compose build
$> docker-compose up
````

2- Use the IntelliJ IDE to start the project or with maven: `mvn spring-boot:run`

### To-do list (improvements)

I haven't finished this, I had a lot of meetings and alignments in my job :(

1. Add tests
2. Use cache (cluster)
3. Orchestrate docker-images
4. Migrates to RPC <--- this is important :)
5. Adds rabbiMQ
6. Uses local-stack for AWS services as SNS, SQS, API Gateway, lambdas

### Please review this comment :point_down:

> *Note*: The final architecture that I was looking for is attached in the GBM.pdf,
> please feel free to ask me whatever you want/need

### Endpoints for testing

* Create an account

`POST http://localhost:8080/accounts`

Request body:

```json
{
    "cash": "10000"
}
```

* Get all accounts

`GET http://localhost:8080/accounts`

* BUY/SELL stocks

`POST http://localhost:8080/accounts/:id/orders`

Request body to buy:

```json
{
    "operation": "BUY",
    "issuers": [
        {
            "issuer_name": "NETFX",
            "total_shares": 100,
            "share_price": 500
        }
    ]
}
```

Request body to sell:

```json
{
    "operation": "SELL",
    "issuers": [
        {
            "issuer_name": "NETFX",
            "total_shares": 100,
            "share_price": 500
        }
    ]
}
```